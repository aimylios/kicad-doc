:author: The KiCad Team
:doctype: book
:toc:
:ascii-ids:

= Pcbnew

_Reference manual_

[[copyright]]
*Copyright*

This document is Copyright (C) 2010-2020 by its contributors as listed
below. You may distribute it and/or modify it under the terms of either
the GNU General Public License  (http://www.gnu.org/licenses/gpl.html),
version 3 or later, or the Creative Commons Attribution License
(http://creativecommons.org/licenses/by/3.0/), version 3.0 or later.

All trademarks within this guide belong to their legitimate owners.

[[contributors]]
*Contributors*

Jean-Pierre Charras, Fabrizio Tappero, Wayne Stambaugh, Jon Evans

[[feedback]]
*Feedback*

Please direct any bug reports, suggestions or new versions to here:

- About KiCad document: https://gitlab.com/kicad/services/kicad-doc/issues

- About KiCad software: https://gitlab.com/kicad/code/kicad/issues

- About KiCad translation: https://gitlab.com/kicad/code/kicad-i18n/issues

include::pcbnew_introduction.adoc[po4a]

include::pcbnew_display_and_selection.adoc[po4a]

include::pcbnew_create_board.adoc[po4a]

include::pcbnew_editing.adoc[po4a]

include::pcbnew_inspecting.adoc[po4a]

include::pcbnew_generating_outputs.adoc[po4a]

include::pcbnew_footprints_and_libraries.adoc[po4a]

include::pcbnew_advanced.adoc[po4a]

include::pcbnew_actions_reference.adoc[po4a]
